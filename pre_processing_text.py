#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 27 14:12:53 2018

@author: Pera
"""


import pandas as pd
from nltk import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk import FreqDist
import pickle

from serbian_stemmer1 import stem

# %%
raw_data = pd.read_csv('articles.csv')

# %%
corpus_of_sentences = []
line = 0


stop_words=["nakon","nam","nama","nas","naš","naša","naše","našeg","ne","nego",
            "neka","neki","nekog","neku","nema","netko","neće","nećemo",
            "nećete","nećeš","neću","nešto","ni","nije","nikoga","nikoje",
            "nikoju","nisam","nisi","nismo","niste","nisu","njega","njegov",
            "njegova","njegovo","njemu","njezin","njezina","njezino","njih",
            "njihov","njihova","njihovo","njim","njima","njoj","nju","no","o",
            "od","odmah","on","ona","oni","ono","ova","pa","pak","po","pod",
            "pored","prije","s","sa","sam","samo","se","sebe","sebi","si",
            "smo","ste","su","sve","svi","svog","svoj","svoja","svoje","svom",
            "ta","tada","taj","tako","te","tebe","tebi","ti","to","toj","tome",
            "tu","tvoj","tvoja","tvoje","u","uz","vam","vama","vas","vaš",
            "vaša","vaše","već","vi","vrlo","za","zar","će","ćemo","ćete",
            "ćeš","ću","što","ali","a","do","od","još","na","je","i","ili","ima",
            "jer","ako","ga","da","iz","li","bi","biste","bili","bih","bilo","bila","koji","koja","kako","koje","šta","pre","bio",
            "im","ih","mu","njoj","njima","ovom","tog","ovog","ka","tog","ovog","ovo","onog","inače","ovim","tih","takav","kakav",
            "kao","njen","pri","oko"]


def pre_processing(raw_data):
    words = []
    list_of_sentences = []
    #def pre_processing(raw_data):
    list_of_sentences = []
    for ind,row in raw_data.iterrows():
        raw_data.loc[ind]['content'] = sent_tokenize(str(raw_data.loc[ind]['content'] ))
        
        for sentence in raw_data.loc[ind]['content']:
    
            words_from_sentence = word_tokenize(sentence.lower())
            words_from_sentence=[word for word in words_from_sentence if word.isalpha()]
            
            list_of_sentences.append(words_from_sentence)
        if ind%1000 == 0:
            print(ind)
    
    print(list_of_sentences)
    for ind_sent,sent in enumerate(list_of_sentences):
        cl = []
        for word in sent:
            if word not in stop_words:
                word = stem(word)[:-1]
                cl.append(word)
                words.append(word)
        if ind_sent%1000 == 0:
            print(ind_sent)
                
            

        list_of_sentences[ind_sent] = cl
    

    print(len(list_of_sentences))
    
    fdist=FreqDist()
   
    for word in words:

        fdist[word] += 1
 
    most_freq_words = fdist.most_common(20)
    print(most_freq_words)
    
    # re-cleaning sentences, to delete non - frequent words
    
    words = [word[0] for word  in most_freq_words]
   
    for ind, sentence in enumerate(list_of_sentences):
    
        cleaned_sentence = []

        for word in sentence:


            if word in words:
                cleaned_sentence.append(word)

        list_of_sentences[ind] = cleaned_sentence
    
    
    return words, list_of_sentences

words, list_of_sentences = pre_processing(raw_data)

# %%
list_of_sentences_cl = []

for ind,sent in enumerate(list_of_sentences):
    if len(sent)!=0:
        list_of_sentences_cl.append(sent)


with open('words_list_of_sentences_29_05_2018_1.dat', 'wb') as f:
    pickle.dump([words, list_of_sentences_cl], f)
# %%    

