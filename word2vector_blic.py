#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 27 20:54:37 2018



@author: Pera
"""

import pickle 
import numpy as np
import pandas as pd
import math 
from matplotlib import pyplot as plt
import tensorflow as tf


#Open file containing scraped text from www.blic.rs , related to news/politics
#Texts are cleaned, stemmed with serbian language stemmer. 

with open('words_list_of_sentences_29_05_2018_1.dat','rb') as f:
    [words,list_of_sentences]=pickle.load(f)
    


# %%

#dictionaries: word2int will map each word to unique intiger, and
# int2word / vice versa

word2int = {}
int2word = {}

# gives the total number  words

vocab_size = len(words)

# fill the dics

for i,word in enumerate(words):
    word2int[word] = i
    int2word[i] = word

# %%

def one_hot(ind,vocab_size):
    'for every word (ind) returnse one hot encoded vector'
    rec = np.zeros(vocab_size)
    rec[ind] = 1
    return rec


#parameter of model for building training set
WINDOW_SIZE = 1

# training sets (conext word - soroundings)

data_recs = []


for sent in list_of_sentences:
    for ind, w in enumerate(sent):
        for nb_w in sent[max(ind - WINDOW_SIZE, 0) : min(ind + WINDOW_SIZE, len(sent)) + 1] :
            if nb_w != w:
                data_recs.append([w,nb_w])

# %%
x_train,y_train = [], []
for rec in data_recs:
    x_train.append(word2int[ rec[0] ])
    y_train.append(word2int[ rec[1] ])

# %%
skip_gram_pairs = []

for ind,val in enumerate(x_train):
    skip_gram_pairs.append([val,y_train[ind]])
    
    
# %%
def get_skipgram_batch(batch_size):
    instance_indices = list(range(len(skip_gram_pairs)))
    np.random.shuffle(instance_indices)
    batch = instance_indices[:batch_size]
    x = [skip_gram_pairs[i][0] for i in batch]
    y = [[skip_gram_pairs[i][1]] for i in batch]
    return x, y


# %%

batch_size = 10000
emb_dims = 20
negative_samples = 2000

train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
train_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])


with tf.name_scope("embeddings"):

    embeddings = tf.Variable(
       tf.random_uniform([vocab_size, emb_dims],
                         -1.0, 1.0), name='embedding')
    # This is essentially a lookup table
    embed = tf.nn.embedding_lookup(embeddings, train_inputs)


nce_weights = tf.Variable(
            tf.truncated_normal([vocab_size, emb_dims],
                                stddev=1.0 / math.sqrt(emb_dims)))
nce_biases = tf.Variable(tf.zeros([vocab_size]))

loss = tf.reduce_mean(
        tf.nn.nce_loss(weights=nce_weights, biases=nce_biases, inputs=embed,
                       labels=train_labels, num_sampled=negative_samples,
                       num_classes=vocab_size))

global_step = tf.Variable(0, trainable=False)
learningRate = tf.train.exponential_decay(learning_rate=0.8,
                                          global_step=global_step,
                                          decay_steps=1000,
                                          decay_rate=0.9,
                                          staircase=True)

train_step = tf.train.GradientDescentOptimizer(learningRate).minimize(loss)

merged = tf.summary.merge_all()
previous_loss = 10000

loss_history = []


with tf.Session() as sess:

    tf.global_variables_initializer().run()


    for step in range(1000):

        x_batch, y_batch = get_skipgram_batch(batch_size)

        sess.run(train_step, feed_dict={train_inputs: x_batch,
                                              train_labels: y_batch})

        loss_value = sess.run(loss, feed_dict={train_inputs: x_batch,
                                                 train_labels: y_batch})

        loss_history.append(loss_value)

        if step%100 == 0:
            plt.plot(loss_history)
            plt.show()

        if loss_value < previous_loss:

            # save the model if it is improved 
            norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings),
                                         1, keep_dims=True))
            normalized_embeddings = embeddings / norm
            normalized_embeddings_matrix = sess.run(normalized_embeddings)

            with open('normalized_embeddings_matrix_20_dimensions.emb', 'wb') as f:

                list_of_variables = [words, word2int, int2word,
                                     normalized_embeddings_matrix]
                pickle.dump(list_of_variables, f)

            print(loss_value)

            previous_loss = loss_value